print('{0}, {1}, {2}'.format('a', 'b', 'c'))
print('{}, {}, {}'.format('a', 'b', 'c'))
print('{2}, {1}, {0}'.format(*'abc'))
print('{0}{1}{0}'.format('abra', 'cad'))
print('Coordinates: {latitude}, {longitude}'.format(latitude='37.24N', longitude='-115.81W'))
coord = {'latitude': '37.24N', 'longitude': '-115.81W'}
print('Coordinates: {latitude}, {longitude}'.format(**coord))

coord = (3, 5)
'X: {0[0]};  Y: {0[1]}'.format(coord)
"repr() shows quotes: {!r}; str() doesn't: {!s}".format('test1', 'test2')
print('{:<30}'.format('left aligned'))
print('{:>30}'.format('right aligned'))
print('{:^30}'.format('centered'))

# спецификация ::=  [[fill]align][sign][#][0][width][,][.precision][type]
# заполнитель  ::=  символ кроме '{' или '}'
# выравнивание ::=  "<" | ">" | "=" | "^"
# знак         ::=  "+" | "-" | " "
# ширина       ::=  integer
# точность     ::=  integer
# тип          ::=  "b" | "c" | "d" | "e" | "E" | "f" | "F" | "g" | "G" |
#                   "n" | "o" | "s" | "x" | "X" | "%"

print('{:*^30}'.format('centered'))  # use '*' as a fill char
print('{:+f}; {:+f}'.format(3.14, -3.14))  # show it always
print('{: f}; {: f}'.format(3.14, -3.14))  # show a space for positive numbers
print('{:-f}; {:-f}'.format(3.14, -3.14))  # show only the minus -- same as '{:f}; {:f}'
# format also supports binary numbers
print("int: {0:d};  hex: {0:x};  oct: {0:o};  bin: {0:b}".format(42))
# with 0x, 0o, or 0b as prefix:
print("int: {0:d};  hex: {0:#x};  oct: {0:#o};  bin: {0:#b}".format(42))

points = 19.5
total = 22
print('Correct answers: {:.3%}'.format(points/total))
