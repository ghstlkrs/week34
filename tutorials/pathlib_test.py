from pathlib import *
current_dir = Path.cwd()
home_dir = Path.home()

print(current_dir)
print(home_dir)

cat_path = Path.cwd() / 'cats' / 'black_cat.txt'
print(cat_path)

kitty_path = Path.cwd() / 'cats' / 'kitties' / 'white_kitty.txt'
print(kitty_path)

print(Path.stat(kitty_path))

p = Path()
print(p)
print([x for x in p.iterdir() if x.is_dir()])

print([x for x in p.glob('*.py')])

# drive: строка, что представляет название жесткого диска. К примеру, PureWindowsPath('c:/Program Files/CSV').drive вернет "C:";
# parts: возвращает кортеж, что дает доступ к компонентам пути;
# name: компонент пути без директории;
# parent: последовательность обеспечивает доступ к логическим предкам пути;
# stem: финальный компонент пути без суффикса;
# suffix: разрешение файла финального компонента;
# anchor: часть пути перед директорией. / используется для создания дочерних путей и имитации поведения os.path.join;
# joinpath: совмещает путь с предоставленными аргументами;
# match(pattern): возвращает True/False, основываясь на совпадении пути с предоставленным шаблоном поиска.
# Например, у нас есть данный путь "/home/projects/pyscripts/python/sample.md":
#
# path: — возвращает PosixPath(‘/home/projects/pyscripts/python/sample.md’);
# path.parts: — возвращает (‘/’, ‘home’, ‘projects’, ‘pyscripts’, ‘python’);
# path.name: — возвращает ‘sample.md’;
# path.stem: — возвращает ‘sample’;
# path.suffix: — возвращает ‘.md’;
# path.parent: — возвращает PosixPath(‘/home/projects/pyscripts/python’);
# path.parent.parent: — возвращает PosixPath(‘/home/projects/pyscripts’);
# path.match('*.md'): возвращает True;
# PurePosixPath('/python').joinpath('edited_version'): возвращает (‘home/projects/pyscripts/python/edited_version.