import csv
from pathlib import *
from jinja2 import *


def generate_csv_rows(data, name):
    for row in data:
        if row['name'] == name:
            yield row


def process_csv(csvpath, name):
    csvfullpath = Path.cwd() / Path(csvpath)

    try:
        with open(csvfullpath, newline='', encoding='utf-8') as file:
            msg = 'File opened\n'
            name_strip = name.strip()
            data = csv.DictReader(file)
            dict_result = sorted(generate_csv_rows(data, name_strip), key=lambda column: int(column['age']))

            if len(dict_result) != 0:
                file.seek(0)
                headers = next(csv.reader(file))  # + попробовать перевести через словарь
                result = []
                for elem in dict_result:
                    temp_list = []
                    for header in headers:
                        temp_list.append((elem[header]))
                    result.append(temp_list)

                template_data = {'rows': result, 'headers': headers, 'name': name_strip}

            else:
                msg += f'Name {name_strip} not found\n'
                template_data = None

    except Exception as exp:
        msg = str(exp) + '\n'
        template_data = None

    return msg, template_data


def create_html(htmlpath, data):
    try:
        if data is not None:
            new_html_path = Path.cwd() / htmlpath / Path(f'{data["name"]}.html')
            with open(new_html_path, 'w', encoding='utf-8') as file:
                print('Started HTML creation')
                template_loader = FileSystemLoader(Path('../templates'))
                template_env = Environment(loader=template_loader)
                template = "cats.html"
                template = template_env.get_template(template)
                file.write(template.render(data))
                msg = f'HTML "{new_html_path}" created\n'
        else:
            msg = 'HTML was not created\n'
    except Exception as exp:
        msg = str(exp)

    return msg


def csv_to_html(name, csvpath, htmlpath):
    msg, data = process_csv(csvpath, name)
    print(msg, end='')
    print(create_html(htmlpath, data))


csv_to_html('Мурка', 'cats/cats.csv', 'cats')

