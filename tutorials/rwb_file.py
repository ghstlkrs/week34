from pathlib import Path
import pickle

from generators import dict_values


def bin_file(bfile_path, dictionary):
    """
    Создает бинарный файл и записывает в него значения из заданного словаря, затем читает данные из полученного файла.
    :param bfile_path:
    :param dictionary:
    :return:
    """
    try:
        with open(bfile_path, 'wb') as file:
            msg = 'Файл открыт успешно.\n'
            pickle.dump([x for x in dict_values(dictionary)], file)
            msg += 'Запись в файл успешно завершена.\n'
    except FileNotFoundError:
        msg = 'Файл не найден.'

    try:
        with open(bfile_path, 'rb') as file:
            data = pickle.load(file)
    except FileNotFoundError:
        msg = 'Файл не найден.'
        data = 'Произошла ошибка при чтении файла.'
    else:
        msg += 'Файл успешно прочитан.'

    return msg, data


bfile_path = Path.cwd() / 'dogs' / 'dogs.bin'

my_dict = {x: f'dog{x}' for x in range(10)}
print(my_dict)

msg, data = bin_file(bfile_path, my_dict)
print(msg)
print(data)
