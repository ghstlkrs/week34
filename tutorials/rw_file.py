from pathlib import Path


def read_and_write(file_r_path, file_w_path):
    """
    Читает символы в файле через один и записывает получившуюся строку в новый файл.
    :param file_w_path: Путь до файла записи
    :param file_r_path: Путь до файла чтения
    :return: Возвращает сообщения о процессах в функции
    """
    try:
        with open(file_r_path, 'r', encoding='utf-8') as file:
            msg = 'Файл открыт успешно.\n'
            result_str = ''

            while True:
                my_char = file.read(1)
                file_eof = file.read(1)
                result_str += my_char
                if file_eof == '':
                    msg += 'Файл прочитан до конца, либо был пуст.\n'
                    break

        try:
            with open(file_w_path, 'w', encoding='utf-8') as file:
                file.write(result_str)
                msg += 'Запись в новый файл успешно завершена.\n'
        except Exception as exp:
            msg += str(exp)

    except Exception as exp:
        msg = str(exp)

    return msg


file_r_path = Path.cwd() / 'cats' / 'kitties' / 'white_kitty.txt'
file_w_path = Path.cwd() / 'cats' / 'kitties' / 'grey_kitty.txt'

print(read_and_write(file_r_path, file_w_path))
