from functools import reduce

multiply = lambda x, y: x * y
print(multiply(1230, 213454302))
print('')

old_list = ['1', '2', '3', '4', '5', '6', '7']
new_list = list(map(int, old_list))
print(new_list)
print('')

mile_distances = [1.0, 6.5, 17.4, 2.4, 9]
kilometer_distances = list(map(lambda x: x * 1.6, mile_distances))
print(kilometer_distances)
print('')

l1 = [1, 2, 3]
l2 = [4, 5, 6]
new_list = list(map(lambda x, y: x + y, l1, l2))    # кол-во аргументов совпадает с кол-вом списков
print(new_list)
print('')

mixed = ['мак', 'просо', 'мак', 'мак', 'просо', 'мак', 'просо', 'просо', 'просо', 'мак']
zolushka = list(filter(lambda x: x == 'мак', mixed))
print(zolushka)
print('')

# Возвращает единичное пользуясь условием в функции-аргументе
items = [1, 24, 17, 14, 9, 32, 2]
all_max = reduce(lambda a, b: a if (a > b) else b, items)
print(all_max)
print('')

a = [1, 2, 3]
b = "xyz"
c = (None, True)
res = list(zip(a, b, c))
print(res)
