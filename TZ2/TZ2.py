import csv
from pathlib import *
from jinja2 import *


def generate_csv_rows(data, town):
    for row in data:
        if row['town'] == town:
            yield row


def process_csv(csvpath, town):
    csvfullpath = Path(csvpath)

    try:
        with open(csvfullpath, newline='', encoding='utf-8') as file:
            msg = 'File opened\n'
            town_strip = town.strip()
            data = csv.DictReader(file)
            dict_result = sorted(generate_csv_rows(data, town_strip), key=lambda column: column['day'])

            if len(dict_result) != 0:
                file.seek(0)
                headers = next(csv.reader(file))
                result = []
                for elem in dict_result:
                    temp_list = []
                    for header in headers:
                        temp_list.append((elem[header]))
                    result.append(temp_list)

                template_data = {'rows': result, 'headers': headers, 'town': town_strip}

            else:
                msg += f'Town {town_strip} not found\n'
                template_data = None

    except Exception as exp:
        msg = str(exp) + '\n'
        template_data = None

    return msg, template_data


def create_html(htmlpath, data):
    try:
        if data is not None:
            new_html_path = Path(htmlpath) / Path(f'{data["town"]}.html')
            with open(new_html_path, 'w', encoding='utf-8') as file:
                print('Started HTML creation')
                template_loader = FileSystemLoader(Path('../templates'))
                template_env = Environment(loader=template_loader)
                template = "weather.html"
                template = template_env.get_template(template)
                file.write(template.render(data))
                msg = f'HTML "{new_html_path}" created\n'
        else:
            msg = 'HTML was not created\n'
    except Exception as exp:
        msg = str(exp)

    return msg


def csv_to_html(town, csvpath, htmlpath):
    msg, data = process_csv(csvpath, town)
    print(msg, end='')
    print(create_html(htmlpath, data))


csv_to_html('Лондон', '../TZ2/weather.csv', '../TZ2')
csv_to_html('Москва', '../TZ2/weather.csv', '../TZ2')
csv_to_html('Канберра', '../TZ2/weather.csv', '../TZ2')
csv_to_html('Стокгольм', '../TZ2/weather.csv', '../TZ2')
csv_to_html('Прага', '../TZ2/weather.csv', '../TZ2')

